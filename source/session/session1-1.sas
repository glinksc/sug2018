﻿/* SAS日本語版で実行 */
ods _all_ close;
ods listing;
libname _tmp 'c:\temp';

data _tmp.test(label='テストDS');
  attrib var1 length=$10 label='変数X';
  var1='あいうえお';
run;

data work;
  set _tmp.test;
  var2=cats(var1,var1);
run;

/* 正しく表示される */
proc print;run;
ods listing close;
