﻿libname _entry 'X:\Project01\Check\Rawdata' access=readonly;
libname _out 'X:\Project01\Check\Output' access=readonly;
filename chklst 'X:\Project01\Check\Checklist\ checklist.xlsx';
%inc 'X:\Project01\Check\SASprg\macro.sas' /source2;

%getpath(0);

%inc "&execdir.\sub1.sas' /source2;
%inc "&execdir.\sub2.sas' /source2;

%Logprint;
