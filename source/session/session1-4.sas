﻿/* SAS Unicodeサポートで実行 */
ods _all_ close;
ods listing;
libname _tmp 'c:\temp' cvpmult=2;

data work;
  set _tmp.test;
  var2=cats(var1,var1);
run;

/* 正しく表示される */
proc print;run;
ods listing close;
