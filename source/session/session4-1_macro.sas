﻿/* 実行時のプログラム名とフォルダ階層を取得するマクロ */
%MACRO getpath(lv,subdir);
  %global execpath execdir logpath lstpath;

  %LET execpath=%SYSFUNC(GETOPTION(SYSIN));
  %IF %LENGTH(&execpath)=0
  %THEN %LET execpath=%SYSGET(SAS_EXECFILEPATH);
  
  %let logpath=%ksubstr(&execpath.,1,%kindex(&execpath.||"\\",".sas\\")-1)||'.log';
  %let lstpath=%ksubstr(&execpath.,1,%kindex(&execpath.||"\\",".sas\\")-1)||'.lst';
  %do _i=0 %to &lv.;
    %let prgname=%kscan(&execpath.,-1,'\');
    %let prgidx=%kindex(&execpath.||"\\",&prgname.||"\\");
    %let execpath=%ksubstr(&execpath.,1,&prgidx.-2);
  %end;
  %let execdir=&execpath.&subdir.;
  
%MEND getpath;


/* ログとアウトプットを作成するマクロ */
%Macro Logprint;
%if "&SYSPROCESSMODE."="SAS DMS Session" %then %do;
  dm log 'print file="&logpath." replace';
  dm output 'print file="&lstpath." replace';
%end;

%Mend Logprint;
