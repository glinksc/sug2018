﻿/* SAS Unicodeサポートで実行 */
ods _all_ close;
ods listing;
data test(label='テストDS');
  attrib var1 length=$10 label='変数X';
  var1='あいうえお';
run;

data work;
  set test;
  var2=cats(var1,var1);
run;

/* 正しく表示されない */
proc print;run;
ods listing close;
