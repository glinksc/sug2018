﻿libname _RAWDATA 'c:\temp\sug\demo';
proc format;
  value sexf
    1='男'
    2='女'
    9='その他';
  ;
  value pointf
    0='開始前'
    1='1週後'
    2='2週後'
    3='3週後'
    4='4週後'
    5='終了後'
  ;
run;

data _null_;
  set _RAWDATA.seed;
  call symputx('SEED',seed);
run;

/* 適当に症例データを作成する */
data caselist;
  attrib id    length=$4 label='ID';
  attrib sex   length=8  label='性別';
  attrib point length=8  label='時点';
  attrib val   length=8  label='検査値';
  do i=1 to 300;
    id   =put(int(i/6)+1,z4.);
    if mod(input(id,best.),9)=0 then sex=9; 
    else
    sex  =mod(int(i/6)+1,2)+1;
    point=mod(i-1,6);
    val  =int(ranuni(&seed)*100);
    output;
  end;
  drop i;
run;
proc sort;
  by id point;
run;

data check1;
  set caselist;
  if sex^=1 then do;
    if val<50 then output;
  end;
  else
  do;
    if val>=90 then output;
  end;
run;


ods _all_ close;
ods listing;
proc print;
  format sex sexf.
         point pointf.;
run;
ods listing close;
